<?php

function get_file($thing) {
    if (class_exists($thing)) {
        $reflector = new ReflectionClass($thing);
    } elseif (is_callable($thing)) {
        $reflector = new ReflectionFunction($thing);
    } else {
        return false;
    }
    return $reflector->getFileName();
}
